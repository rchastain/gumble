default: test/PerftSuite.cmake build-debug build

test/PerftSuite.cmake: ext/perftsuite.epd
	tr -d '\r' < $< | awk '{print $$NF,$$0}' | sort -nr | cut -d' ' -f2- | ./ext/perftsuite-to-ctest > $@

build-debug:
	mkdir $@
	cd $@ && CFLAGS="-g -Og -Wall" cmake .. || (rm -R ../$@; exit 1)

# Use gcc for optimized build, it is noticeably better than clang
build:
	mkdir $@
	cd $@ && CC=gcc CFLAGS="-Ofast -march=native -flto -DNDEBUG" cmake .. || (rm -R ../$@; exit 1)

clean:
	rm -f test/PerftSuite.cmake
	rm -Rf build build-debug

.PHONY: default clean
