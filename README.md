Gumble
======

A stupid toy chess engine. Released under the Apache License 2.0.

![](https://giant.gfycat.com/MemorableAchingEmperorshrimp.webm)

Compiling
=========

Gumble requires `clang` or `gcc` and CMake. Other compilers may work
but have not been tested.

~~~
make
cd build && make                    # optimized build
cd build-debug && make              # debug build
cd build-debug && make && ctest -j8 # run tests
~~~

Using Gumble
============

The `src/gumble` binary implements parts of the UCI protocol. It can
be used with any chess GUI that supports UCI engines :

* Scid, Scid vs PC
* Cute Chess
* XBoard with Polyglot
* etc.

Technical mumbo jumbo
=====================

* 8x8 board representation with bitboards
* negamax search with alpha-beta pruning
* iterative deepening

Possible improvements
---------------------

* threefold repetition
* transposition table
* quiescence search

UCI extras
==========

The `gumble` binary implements a few non-standard UCI commands.

`fen`
-----

Dump the current game (as set by the position command) as a FEN string.

~~~
> position startpos moves e2e4
> fen
< info fen rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
~~~

`lan`
-----

Parse a short algebraic notation (SAN) move and outputs its long variant.

~~~
> position startpos
> lan e4
< info lan e2e4
> lan Nf3
< info lan g1f3
> position fen r2q1rk1/1p2b1pp/p7/2Pb1p2/P3N3/5Q2/1P3PPP/R4RK1 w - - 0 18
> lan Qxf5
< info lan f3f5
~~~

`san`
-----

Parse a long algebraic notation (LAN) move and outputs its short
variant. Includes check (+) and checkmate (#).

~~~
> position startpos
> san g1f3
< info san Nf3
> position fen 6k1/p1p2p1p/4q3/3p2pP/1B3PP1/P5KR/1r6/8 b - - 0 44
> san g5f4
< info san gxf4+
> san e6e3
< info san Qe3#
~~~

`moves`
-------

Output the LANs of all possible legal moves for the current position.

~~~
> position startpos
< moves
> info moves a2a3 a2a4 b1a3 b1c3 b2b3 b2b4 c2c3 c2c4 d2d3 d2d4 e2e3 e2e4 f2f3 f2f4 g1f3 g1h3 g2g3 g2g4 h2h3 h2h4
~~~

Credits
=======

* The [Chess Programming
Wiki](https://chessprogramming.wikispaces.com/) as a whole.

* Helpful people of `##chessprogramming` on Freenode.

* The `perftsuite.epd` file can be found at
<http://www.rocechess.ch/perft.html>. Other examples were taken from
the [CPW](https://chessprogramming.wikispaces.com/Perft+Results).

* The UCI specification (`engine-interface.txt`) was taken from
<http://www.shredderchess.com/chess-info/features/uci-universal-chess-interface.html>.
