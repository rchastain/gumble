/* Copyright 2018 Romain "Artefact2" Dal Maso <artefact2@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "gumble.h"

static cch_hash_t cch_rand(void) {
	/* Slow and inefficient, but it is portable */
	cch_hash_t ret = 0;

	for(size_t i = 0; i < sizeof(cch_hash_t); ++i) {
		ret <<= 8;
		ret |= rand() / (RAND_MAX >> 8);
	}

	return ret;
}

int main(void) {
	srand(time(0));
	puts("static const cch_hash_t cch_zobrist_pieces[][64] = {");
	for(unsigned char i = 0; i < 12; ++i) {
		putchar('{');
		for(unsigned char sq = 0; sq < 64; ++sq) {
			printf("%lluULL, ", cch_rand());
		}
		puts("},");
	}
	puts("};");

	printf("static const cch_hash_t cch_zobrist_ep[] = { %lluULL, %lluULL, %lluULL, %lluULL, %lluULL, %lluULL, %lluULL, %lluULL };\n",
		   cch_rand(), cch_rand(), cch_rand(), cch_rand(), cch_rand(), cch_rand(), cch_rand(), cch_rand());
	printf("static const cch_hash_t cch_zobrist_castles[] = { %lluULL, %lluULL, %lluULL, %lluULL };\n", cch_rand(), cch_rand(), cch_rand(), cch_rand());
	printf("static const cch_hash_t cch_zobrist_side = %lluULL;\n", cch_rand());
	return 0;
}
