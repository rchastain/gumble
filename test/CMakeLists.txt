add_executable(debugcli debugcli.c)
target_link_libraries(debugcli enginecore)
target_compile_definitions(debugcli PRIVATE _POSIX_C_SOURCE=200809)

macro(ch_test NAME)
add_executable(${NAME} ${NAME}.c)
target_link_libraries(${NAME} enginecore)
add_test(${NAME} ${NAME})
endmacro(ch_test)

ch_test(fen)
ch_test(algebraic)

add_executable(perft perft.c)
target_link_libraries(perft enginecore)
target_compile_definitions(perft PRIVATE BULK=0)

add_executable(perft-bulk perft.c)
target_link_libraries(perft-bulk enginecore)
target_compile_definitions(perft-bulk PRIVATE BULK=1)

# https://chessprogramming.wikispaces.com/Perft+Results

add_test(perft-cpw-1 perft "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" 5)
set_tests_properties(perft-cpw-1 PROPERTIES PASS_REGULAR_EXPRESSION
							 "perft\\(5\\)=4865609 nodes, 82719 captures, 258 ep, 0 castles, 0 promotions, 27351 checks\n")


add_test(perft-cpw-2 perft "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -" 4)
set_tests_properties(perft-cpw-2 PROPERTIES PASS_REGULAR_EXPRESSION
							 "perft\\(4\\)=4085603 nodes, 757163 captures, 1929 ep, 128013 castles, 15172 promotions, 25523 checks\n")


add_test(perft-cpw-3 perft "8/2p5/3p4/KP5r/1R3p1k/8/4P1P1/8 w - -" 6)
set_tests_properties(perft-cpw-3 PROPERTIES PASS_REGULAR_EXPRESSION
							 "perft\\(6\\)=11030083 nodes, 940350 captures, 33325 ep, 0 castles, 7552 promotions, 452473 checks\n")


add_test(perft-cpw-4 perft "r3k2r/Pppp1ppp/1b3nbN/nP6/BBP1P3/q4N2/Pp1P2PP/R2Q1RK1 w kq - 0 1" 4)
set_tests_properties(perft-cpw-4 PROPERTIES PASS_REGULAR_EXPRESSION
							 "perft\\(4\\)=422333 nodes, 131393 captures, 0 ep, 7795 castles, 60032 promotions, 15492 checks\n")


add_test(perft-cpw-5 perft-bulk "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8" 5)
set_tests_properties(perft-cpw-5 PROPERTIES PASS_REGULAR_EXPRESSION "perft\\(5\\)=89941194 nodes\n")


add_test(perft-cpw-6 perft-bulk "r4rk1/1pp1qppp/p1np1n2/2b1p1B1/2B1P1b1/P1NP1N2/1PP1QPPP/R4RK1 w - - 0 10" 5)
set_tests_properties(perft-cpw-6 PROPERTIES PASS_REGULAR_EXPRESSION "perft\\(5\\)=164075551 nodes\n")

include(PerftSuite.cmake)
